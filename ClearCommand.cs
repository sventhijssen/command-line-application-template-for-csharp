﻿using System;

namespace ConsoleAppTemplate
{
    class ClearCommand : ICommand
    {
        public bool Execute()
        {
            Console.Clear();
            return false;
        }
    }
}