﻿namespace ConsoleAppTemplate
{
    public interface ICommand
    {
        bool Execute();
    }
}